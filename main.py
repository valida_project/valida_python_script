#%%
%reload_ext autoreload
%autoreload 2
#%%
from os import sep
import numpy as np
from myUart import Uart
import time
import matplotlib.pyplot as plt
#%%
"""-------------------------------------------------------------------------------

  Crea puerto y lo abre

-------------------------------------------------------------------------------"""
uart = Uart(com=8, br=230400, ext_ram_len=4096)
uart.open()

#%%
"""-------------------------------------------------------------------------------

  Cierra puerto

-------------------------------------------------------------------------------"""
uart.close()

#%%
""" flush """
uart.flush()

#%%
"""-------------------------------------------------------------------------------

  Test de comando de Are You There?

-------------------------------------------------------------------------------"""
uart.api_are_you_there(verbose=False)


#%%
"""-------------------------------------------------------------------------------

 Test de AFE con CSV

-------------------------------------------------------------------------------"""
file_name = 'sound_bed'

#####################################################################
csv = np.loadtxt(f"Input/{file_name}.txt").astype(int)
print(f'{csv.max()=}')
print(f'{csv.min()=}')
plt.figure(1)
plt.plot(csv)
plt.title(f'Audio CSV file {file_name}')

RTL_VALUE_N = 32
RTL_VALUE_L = 128
afe_out = uart.api_afe_test(csv, RTL_VALUE_N, RTL_VALUE_L)
afe_out = afe_out.transpose()

""" compare graphically """
# MIN_DATA = -128
# MAX_DATA = 127
gt_out = np.loadtxt(f"Input/{file_name}_result.csv", delimiter=',')
gt_out = gt_out.transpose()
plt.figure(2)
# plt.imshow(gt_out, vmin=MIN_DATA, vmax=MAX_DATA)
plt.imshow(gt_out)
plt.title('AFE SOFTWARE')
cbar = plt.colorbar()

# afe_to_plot = afe_out[:gt_out.shape[1]]
afe_to_plot = afe_out[:,1:gt_out.shape[1]+1]

plt.figure(3)
plt.rcParams["figure.figsize"] = (10,5)
plt.imshow(afe_to_plot)
plt.title(f'AFE FPGA')
cbar = plt.colorbar()

err = gt_out-afe_to_plot
plt.figure(4)
plt.imshow(err)
plt.title('ERROR')
cbar = plt.colorbar()

#%%
"""-------------------------------------------------------------------------------

  External Buffer Audio RAM

-------------------------------------------------------------------------------"""
#region
ext_ram_len = uart.get_ext_ram_len()


print(f'Test RAM : ')

data_tx = np.random.randint(0, 255, size=(ext_ram_len))

# data_tx = np.linspace(0, ext_ram_len/16, num=ext_ram_len);


# time        = np.linspace(0, 10, num=ext_ram_len);
# data_tx = np.array(np.sin(time)*127+127).round(0).astype(int)

data_tx = np.array(data_tx).astype(int)
data_tx = data_tx & (0xFF)

print(f'Writing {len(data_tx)} bytes...', end = '')
uart.api_ExtRAM_write(data_tx, verbose=False)
print(f'ok')

data_rx = uart.api_ExtRAM_read(verbose=False)
print(f'Reading {len(data_rx)} bytes...', end = '')
print(f'ok')

print('Verifying tx and rx data are the same...', end='')
same_data_flag = np.all(data_tx==data_rx)
if same_data_flag:  print('ok')
else : print('ERROR')
# #%%
# plt.plot(data_tx)
# plt.plot(data_rx)

# #%%
# """ test where is different """

# diff = abs(data_tx - data_rx)
# plt.plot(diff)
#endregion
#%%
"""-------------------------------------------------------------------------------

  Lectura de AFE

-------------------------------------------------------------------------------"""
#region
n_samples = 128
sample_rate = 8000
RTL_VALUE_N = 32

afe_data = uart.api_afe_get(n_samples, sample_rate)

afe_data_plot = afe_data
afe_data_plot = afe_data_plot.reshape(RTL_VALUE_N, (int(len(afe_data_plot)/RTL_VALUE_N)))
plt.rcParams["figure.figsize"] = (10,5)
plt.imshow(afe_data_plot)
plt.title('AFE Read')
cbar = plt.colorbar()

#%%
"""-------------------------------------------------------------------------------

  Lectura de Audio ORIGINAL

-------------------------------------------------------------------------------"""
n_samples = 4000
sample_rate = 8000

audio_data = uart.api_audio_get(n_samples, sample_rate)
# audio_data = uart.api_audio_cropped_get(n_samples, sample_rate)

plt.figure(1)
plt.plot(audio_data)
plt.title("Audio Original")

def playwWavFromArray(array, sample_rate):
    import sounddevice as sd
    sd.default.samplerate = sample_rate
    sd.play(array, blocking=True)

# audio_data = np.random.randint(0,1000, 20000)
# normalize data
def norm_audio(audio):  
  mean = np.mean(audio)
  audio = np.array(audio) - mean
  max = np.max(np.abs(audio))
  audio_n = np.int16(audio/max * (2**15-1))
  return audio_n

mean = np.mean(audio_data)
print(f'mean  = {mean}')
print(f'std  = {np.std(audio_data)}')
print(f'max  = {np.max(audio_data)}')
print(f'min  = {np.min(audio_data)}')

#%%
"""-------------------------------------------------------------------------------

  Lectura de Audio CROPPED

-------------------------------------------------------------------------------"""
n_samples = 4000
sample_rate = 8000

# audio_data = uart.api_audio_get(n_samples, sample_rate)
audio_data = uart.api_audio_cropped_get(n_samples, sample_rate)

plt.figure(1)
plt.plot(audio_data)
plt.title("Audio Cropped")

def playwWavFromArray(array, sample_rate):
    import sounddevice as sd
    sd.default.samplerate = sample_rate
    sd.play(array, blocking=True)

# audio_data = np.random.randint(0,1000, 20000)
# normalize data
def norm_audio(audio):  
  mean = np.mean(audio)
  audio = np.array(audio) - mean
  max = np.max(np.abs(audio))
  audio_n = np.int16(audio/max * (2**15-1))
  return audio_n

mean = np.mean(audio_data)
print(f'mean  = {mean}')
print(f'std  = {np.std(audio_data)}')
print(f'max  = {np.max(audio_data)}')
print(f'min  = {np.min(audio_data)}')

#%%
""" What sees the AFE in the FPGA"""
audio_data_afe = [sample_i>>8 for sample_i in audio_data]
plt.figure(2)
plt.plot(audio_data_afe)
plt.title("Audio Truncado (Input AFE)")

#%%
"""-------------------------------------------------------------------------------

  Play Audio

-------------------------------------------------------------------------------"""
audio_norm = norm_audio(audio_data)

# plt.figure(2)
# plt.plot(audio_norm)
# plt.title("Audio Normalizado")
playwWavFromArray(audio_norm, 8000)

#%%
"""-------------------------------------------------------------------------------

  Save in pickle

-------------------------------------------------------------------------------"""
import my_pickle
file_name = 'off_x3_gain4_sr8000'
my_pickle.pickle_save(f'Output/{file_name}.pickle', audio_data)


#%%
"""-------------------------------------------------------------------------------

  Escribe audio en wav

-------------------------------------------------------------------------------"""
# import numpy as np
# from scipy.io.wavfile import write

# data = np.random.uniform(-1,1,44100) # 44100 random samples between -1 and 1
# scaled = np.int16(data/np.max(np.abs(data)) * 32767)
# write('test.wav', 44100, scaled)





#%%
"""-------------------------------------------------------------------------------

  Test de comando de guardar en buffer de config

-------------------------------------------------------------------------------"""
IDX_TABLE = 2
table_gain = [ 0, 
               2,
               4,
               8,
               16]
table_offset = [-890, 
               -3561,
               -7117,
               -14240,
               -28478]
# valores a configurar
audio_gain = table_gain[IDX_TABLE]
audio_offset = table_offset[IDX_TABLE]
audio_thresh = 200
"""
audio_resolution:
what bit in lsb is the one to take from audio to AFE.
bit   vector
----------------
0     [7..0]
1     [8..1]
...
8     [15..8]
"""
audio_resolution = 0

##################################################################

# ordena para buffer de config
data_tx = np.arange(64) # dummy data

data_tx[0] = audio_gain & 0xFF
data_tx[1] = (audio_offset>>8) & 0xFF
data_tx[2] = audio_offset & 0xFF
data_tx[3] = (audio_thresh>>8) & 0xFF
data_tx[4] = audio_thresh & 0xFF
data_tx[5] = audio_resolution & 0xFF

#region

uart.api_LUTRAMbuffer_write(data_tx, verbose=False)
data_rx = uart.api_LUTRAMbuffer_read(verbose=False)
same_data_flag = np.all(data_tx==data_rx)
print(f'Test write config buffer appi : ')
print(f'Writing {len(data_tx)} bytes...Reading buffer...\nsame data? = {same_data_flag}')
if same_data_flag:  print('Test passed succesfully')
else : print('Test passed ERROR')
#endregion



#%%
# generation of fichero memoria bin
def list_to_bin(l):
  tmpstr = ''
  for i in l:
    tmpstr = tmpstr + f'{bin(i)[2:].rjust(8, "0")}' + '\n'
  return tmpstr

mem_data = np.arange(512)
mem_data[-1] = 50

mem_data_bin = list_to_bin(mem_data)
f = open ('mem_bin.bin','w')
f.write(mem_data_bin)
f.close()
